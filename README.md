# libjson

libjson 是一款高性能 json 解析库，以 LGPL 协议发布

libjson 内部不分配任何内存，对内存消耗非常少。

libjson 是否为线程安全取决于用户传入的回调函数是否线程安全。libjson 本身是线程安全的。

libjson 目前只支持 json 解码。

libjson 目前使用递归方式解析 json 文本，所以如果 json 对象的层数过深，可能会导致堆栈溢出。这还导致使用 libjson 解析时必须把 json 文本完整读入内存中，限制了 libjson 解析超大 json 对象的能力。libjson 将会在 V2 版本中解决该问题。

## 编译

```
make -f nbproject/Makefile-Release.mk QMAKE= SUBPROJECTS= .build-conf
```

## 测试

```
make -f nbproject/Makefile-Release.mk SUBPROJECTS= .build-tests-conf
```

### 解析测试

```
./build/Release/GNU-Linux-x86/tests/TestFiles/f1 ./tests/test.json 1 p
```

### 性能测试

```
time ./build/Release/GNU-Linux-x86/tests/TestFiles/f1 ./tests/test.json 1000000 n
```
